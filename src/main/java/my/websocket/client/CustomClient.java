package my.websocket.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.messaging.simp.stomp.StompSessionHandler;
import org.springframework.web.socket.client.WebSocketClient;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;
import org.springframework.web.socket.sockjs.client.SockJsClient;
import org.springframework.web.socket.sockjs.client.Transport;
import org.springframework.web.socket.sockjs.client.WebSocketTransport;

import my.shared.beans.Message;
import my.websocket.client.handler.MyStompSessionHandler;

public class CustomClient {
	/*
	 * Код клиента позаимствован отсюда
	 * https://github.com/jaysridhar/spring-websocket-client/blob/master/src/main/
	 * java/sample/Application.java
	 */

	public static void main(String[] args) throws Exception {
		WebSocketClient simpleWebSocketClient = new StandardWebSocketClient();
		List<Transport> transports = new ArrayList<>(1);
		transports.add(new WebSocketTransport(simpleWebSocketClient));

		SockJsClient sockJsClient = new SockJsClient(transports);
		WebSocketStompClient stompClient = new WebSocketStompClient(sockJsClient);
		stompClient.setMessageConverter(new MappingJackson2MessageConverter());

		String url = "ws://localhost:8080/chat";
		StompSessionHandler sessionHandler = new MyStompSessionHandler();
		StompSession session = stompClient.connect(url, sessionHandler).get();
		Thread.sleep(1000);
		for (int i = 0; i < 1; i++) {
			String text = "Sample message " + i;
			Message msg = new Message(text);
			session.send("/app/chat/topic" + i, msg);
			Thread.sleep(1000);
		}
	}

}

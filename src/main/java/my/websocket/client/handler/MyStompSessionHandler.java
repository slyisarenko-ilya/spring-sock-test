package my.websocket.client.handler;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.simp.stomp.StompFrameHandler;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.messaging.simp.stomp.StompSessionHandlerAdapter;

import my.shared.beans.Message;

public class MyStompSessionHandler extends StompSessionHandlerAdapter {
	Logger logger = LoggerFactory.getLogger(MyStompSessionHandler.class);

	public MyStompSessionHandler() {
	}

	@Override
	public void afterConnected(StompSession session, StompHeaders connectedHeaders) {
		System.err.println("Connected! Headers:");
		showHeaders(connectedHeaders);

		subscribeTopic("/topic/messages", session);
		sendJsonMessage(session);
	}

	/**
	 * Listen /topic/messages ws path
	 * 
	 * @param topic
	 * @param session
	 */
	private void subscribeTopic(String topic, StompSession session) {
		session.subscribe(topic, new StompFrameHandler() {

			@Override
			public Type getPayloadType(StompHeaders headers) {
				logger.info("Get payload type " + headers);
				return Message.class;
			}

			@Override
			public void handleFrame(StompHeaders headers, Object payload) {
				System.err.println(payload.toString());
			}
		});
	}

	private void showHeaders(StompHeaders headers) {
		for (Map.Entry<String, List<String>> e : headers.entrySet()) {
			System.err.print("  " + e.getKey() + ": ");
			boolean first = true;
			for (String v : e.getValue()) {
				if (!first)
					System.err.print(", ");
				System.err.print(v);
				first = false;
			}
			System.err.println();
		}
	}

	private void sendJsonMessage(StompSession session) {
		Message msg = new Message("Hello from spring!");
		session.send("/app/chat/start-topic", msg);
	}

}
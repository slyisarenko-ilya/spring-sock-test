package my.shared.beans;

import java.util.Date;

public class Message {
	private String content;
	private Date date;
	private String topic;

	public Message() {
		
	}
	
	public Message(String content) {
		this.content = content;
	}
		
	public Message(String content, Date date, String topic) {
		super();
		this.content = content;
		this.date = date;
		this.topic = topic;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return "Content: " + getContent() + ", date: " + getDate() +", topic: " + topic;
	}
	
}



package my.application;

import java.util.Calendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

import my.shared.beans.Message;

/**
 * Обрабатывает сообщения, полученные от клиента
 * 
 * @author intense
 */
@Controller
public class MyController {
	Logger logger = LoggerFactory.getLogger(MyController.class);
	
	@MessageMapping("/chat/{topic}")
	@SendTo("/topic/messages") /*на этот адрес клиент принимает ответы сервера 
						       (см. MyStompSessionHandler->subscribeTopic) */
	public Message send(
	        @DestinationVariable("topic") String topic, Message message)
	        throws Exception {
		logger.info("handle /SEND: topic: " + topic +", message: " + message);
	    return new Message("Response from server: " + message.getContent(), Calendar.getInstance().getTime(), topic);
	}

}

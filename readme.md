#### Plan:


* SockJS java client  OK  

* Server with websocket support  OK

* Client send random message to server.  OK

* Server print message, then add some field to message and send it back to client. Ok

* Client print modified message. OK

* No web interface. Only java modules.  OK

* Configured via annotations.  OK



run server: `mvn spring-boot:run`

run client: as console java application

run another client(web): open http://localhost:8080 in browser (spring-boot:run also serves client part) 

---
next:

* same action, but client is full-fledged web interface

* Use SockJS

* Use spring boot with tomcat.

* Configured via xml.
 

--- 

next. 
* Same action, but use spring boot with wildfly.
* Configured via Annotations.

